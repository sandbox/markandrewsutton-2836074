<?php

/**
 * @file
 * Qualtrics API module hook definitions.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Perform an action when a contact is successfully added to a list.
 *
 * @param string $list_id
 *   The Qualtrics list ID.
 * @param string $email
 *   The email address subscribed.
 * @param array $parameters
 *   The paramters used during the subscription.
 * @param object $result.
 *   The result from Qualtrics.
 *
 * @ingroup qualtricsapi
 */
function hook_qualtricsapi_add_contact_success($list_id, $email, $parameters, $result) {}

/**
 * Perform an action when a contact is successfully removed to a list.
 *
 * @param string $list_id
 *   The Qualtrics list ID.
 * @param string $contact_id
 *   The contact ID to remove.
 * @param object $result.
 *   The result from Qualtrics.
 *
 * @ingroup qualtricsapi
 */
function hook_qualtricsapi_remove_contact_success($list_id, $contact_id, $result) {}

/**
 * Perform an action when a distribution is successfully sent.
 *
 * @param object $result
 *   The return value from Qualtrics. Includes the distribution ID.
 *
 * @ingroup qualtricsapi
 */
function hook_qualtricsapi_add_distribution_success($result) {}
