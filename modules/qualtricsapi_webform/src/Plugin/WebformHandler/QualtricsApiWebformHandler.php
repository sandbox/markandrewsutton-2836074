<?php

namespace Drupal\qualtricsapi_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission to Qualtrics handler.
 *
 * @WebformHandler(
 *   id = "qualtricsapi",
 *   label = @Translation("Qualtrics API"),
 *   category = @Translation("Qualtrics API"),
 *   description = @Translation("Sends a form submission to a Qualtrics list."),
 *   cardinality = \Drupal\webform\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class QualtricsApiWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('webform.qualtricsapi')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $lists = qualtricsapi_get_mailing_lists();
    return [
      '#theme' => 'markup',
      '#markup' => '<strong>' . $this->t('List') . ': </strong>' . (!empty($lists[$this->configuration['list']]) ? $lists[$this->configuration['list']]->name : ''),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = \Drupal::config('qualtricsapi.settings');

    return [
      'list' => '',
      'email' => '',
      'first_name' => '',
      'last_name' => '',
      'send_survey_invite' => '',
      'qualtrics_uid' => '',
      'qualtrics_api_user_id' => $config->get('api_user'),
      'survey_info' => [
        'survey' => '',
        'message' => '',
        'message_link_type' => '',
        'email_from' => '',
        'email_from_name' => '',
        'email_replyto' => '',
        'email_subject' => '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $lists = qualtricsapi_get_mailing_lists();
    $surveys = qualtricsapi_get_surveys();
    $messages = qualtricsapi_get_library_messages();

    $options = array();
    $options[''] = $this->t('- Select a list -');
    foreach ($lists as $list) {
      $options[$list->id] = $list->name;
    }

    $form['qualtrics_api_user_id'] = [
      '#type' => 'hidden',
      '#default_value' => $this->configuration['qualtrics_api_user_id'],
    ];

    $form['list'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['list'],
      '#options' => $options,
    ];

    $fields = $this->getWebform()->getElementsFlattenedAndHasValue();

    $options = array();
    $options[''] = $this->t('- Select an email field -');
    foreach ($fields as $field_name => $field) {
      if ($field['#type'] == 'email') {
        $options[$field_name] = $field['#title'];
      }
    }

    $form['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['email'],
      '#options' => $options,
    ];

    // Support name field type as an option for both name fields.
    // Can select text or name field.
    foreach ($fields as $field_name => $field) {
      if ($field['#type'] == 'webform_name') {
        $options[$field_name] = $field['#title'];
      }
    }

    $form['first_name'] = [
      '#type' => 'select',
      '#title' => $this->t('First Name field'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['first_name'],
      '#options' => $options,
    ];

    $form['last_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Last Name field'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['last_name'],
      '#options' => $options,
    ];

    $options = array();
    $options[''] = $this->t('- Select a Qualtrics ID field -');
    foreach ($fields as $field_name => $field) {
      if ($field['#type'] == 'hidden') {
        $options[$field_name] = $field['#title'];
      }
    }

    $form['qualtrics_uid'] = [
      '#type' => 'select',
      '#title' => $this->t('Qualtrics Return ID'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['qualtrics_uid'],
      '#options' => $options,
    ];

    $form['send_survey_invite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send a survey invitation?'),
      '#default_value' => $this->configuration['send_survey_invite'],
    ];

    $form['survey_info'] = [
      '#type' => 'container',
      '#title' => $this->t('Survey Invitation Settings'),
      '#states' => [
        'visible' => [
          'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
        ],
        'required' => [
          'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $options = array();
    $options[''] = $this->t('- Select a survey -');
    foreach ($surveys as $survey) {
      $options[$survey->id] = $survey->name;
    }

    $form['survey_info']['survey'] = [
      '#type' => 'select',
      '#title' => $this->t('Survey'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['survey'],
      '#options' => $options,
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $options = array();
    $options[''] = $this->t('- Select a message -');
    foreach ($messages as $message) {
      $options[$message->id] = $message->description;
    }

    $form['survey_info']['message'] = [
      '#type' => 'select',
      '#title' => $this->t('Library Message'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['message'],
      '#options' => $options,
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $form['survey_info']['message_link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message Link Type'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['message_link_type'],
      '#options' => [
        'Multiple' => 'Multiple',
        'Individual' => 'Individual',
        'Anonymous' => 'Anonymous',
      ],
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $form['survey_info']['email_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email From'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['email_from'],
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $form['survey_info']['email_from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email From Name'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['email_from_name'],
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $form['survey_info']['email_replyto'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply to Email'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['email_replyto'],
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    $form['survey_info']['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['survey_info']['email_subject'],
      'required' => [
        'input[name="settings[send_survey_invite]"]' => ['checked' => TRUE],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        $this->configuration[$name] = $values[$name];
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function preSave(WebformSubmissionInterface $webform_submission) {

    $fields = $webform_submission->toArray(TRUE);

    $email = $fields['data'][$this->configuration['email']];
    $list_id = $this->configuration['list'];
    $drupal_id = $fields['uuid'];

    $parameters = [];

    if (isset($this->configuration['first_name'])) {
      if (is_array($fields['data'][$this->configuration['first_name']])) {
        $parameters['firstName'] = $fields['data'][$this->configuration['first_name']]['first'];
      }
      else {
        $parameters['firstName'] = $fields['data'][$this->configuration['first_name']];
      }
    }
    if (isset($this->configuration['last_name'])) {
      if (is_array($fields['data'][$this->configuration['last_name']])) {
        $parameters['lastName'] = $fields['data'][$this->configuration['last_name']]['last'];
      }
      else {
        $parameters['lastName'] = $fields['data'][$this->configuration['last_name']];
      }
    }

    $response = qualtricsapi_add_contact($list_id, $email, $drupal_id, $parameters);

    if ($response->result->id) {

      drupal_set_message(t('Contact created with id ' . $response->result->id), 'status');

      if (!empty($this->configuration['qualtrics_uid'])) {
        $data = $webform_submission->getData();
        $data[$this->configuration['qualtrics_uid']] = $response->result->id;
        $webform_submission->setData($data);
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if (!$update) {

      if (isset($this->configuration['send_survey_invite'])) {

        $fields = $webform_submission->toArray(TRUE);

        $survey_link = [
          'surveyId' => $this->configuration['survey_info']['survey'],
          'type' => $this->configuration['survey_info']['message_link_type'],
        ];

        $header = [
          'fromEmail' => $this->configuration['survey_info']['email_from'],
          'fromName' => $this->configuration['survey_info']['email_from_name'],
          'replyToEmail' => $this->configuration['survey_info']['email_replyto'],
          'subject' => $this->configuration['survey_info']['email_subject'],
        ];

        $message = [
          'libraryId' => $this->configuration['qualtrics_api_user_id'],
          'messageId' => $this->configuration['survey_info']['message'],
        ];

        $recipients = [
          'mailingListId' => $this->configuration['list'],
          'contactId' => $fields[$this->configuration['qualtrics_uid']],
        ];

        $send_date = format_date(strtotime('+2 minutes'), 'custom', 'c');

      }

      $response = qualtricsapi_add_distribution($survey_link, $header, $message, $recipients, $send_date);

      if ($response->result->id) {
        drupal_set_message(t('Survey invitation has been sent with id ' . $response->result->id), 'status');
      } else {
        drupal_set_message(t('Error trying to send a survey invitation. View the logs. ' . $response->result->id), 'error');
      }

    }
  }

}
