<?php

/**
 * @file
 * Qualtrics API module.
 */

use Drupal\Core\Site\Settings;
use Drupal\Component\Utility\Crypt;
use \Qualtrics\Qualtrics;
use \Qualtrics\QualtricsContacts;
use \Qualtrics\QualtricsDistributions;
use \Qualtrics\QualtricsSurveys;
use \Qualtrics\QualtricsAPIException;
use \Qualtrics\QualtricsResponses;
use \Qualtrics\QualtricsLibraries;

/**
 * Access callback for qualtricsapi submodule menu items.
 */
function qualtricsapi_apikey_ready_access($permission) {
  if (qualtricsapi_get_api_object() && \Drupal::currentUser()->hasPermission($permission)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Instantiates a Qualtrics library object.
 *
 * @return \Qualtrics
 *   Drupal Qualtrics library object.
 */
function qualtricsapi_get_api_object($classname = 'Qualtrics') {
  $qualtricsapi = &drupal_static(__FUNCTION__);
  if (isset($qualtricsapi) && $qualtricsapi instanceof $classname) {
    return $qualtricsapi;
  }

  $config = \Drupal::config('qualtricsapi.settings');

  if ($config->get('test_mode')) {
    $classname = '\Qualtrics\Tests\\' . $classname;
  } else {
    $classname = '\Qualtrics\\' . $classname;
  }

  if (!class_exists($classname)) {
    $msg = t('Failed to load Qualtrics PHP library. Please refer to the installation requirements.');
    \Drupal::logger('qualtricsapi')->error($msg);
    drupal_set_message($msg, 'error');
    return NULL;
  }

  $api_key = $config->get('api_key');
  if (!strlen($api_key)) {
    \Drupal::logger('qualtricsapi')->error('Qualtrics Error: API Key cannot be blank.');
    return NULL;
  }

  $api_user = $config->get('api_user');
  if (!strlen($api_user)) {
    \Drupal::logger('qualtricsapi')->error('Qualtrics Error: API User ID cannot be blank.');
    return NULL;
  }

  $timeout = 60;

  $qualtricsapi = new $classname($api_key, $api_user, $timeout);

  return $qualtricsapi;
}

/**
 * Returns a single list.
 *
 * @param string $list_id
 *   The unique ID of the list provided by Qualtrics.
 *
 * @return array
 *   Array of list data.
 */
function qualtricsapi_get_mailing_list($list_id) {
  $lists = qualtricsapi_get_lists(array($list_id));
  return reset($lists);
}

/**
 * Returns all Qualtrics mailing lists for a given key. Lists are stored in the cache.
 *
 * @param array $list_ids
 *   An array of list IDs to filter the results by.
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return array
 *   An array of list data arrays.
 */
function qualtricsapi_get_mailing_lists($list_ids = array(), $reset = FALSE) {
  $lists = array();
  $cache = \Drupal::cache('qualtricsapi');
  $cached_data = $reset ? NULL : $cache->get('contactlists');

  // Return cached lists.
  if ($cached_data) {
    $lists = $cached_data->data;
  }
  else {
    try {
      /* @var \Qualtrics\QualtricsContacts $qtapi */
      $qtapi = qualtricsapi_get_api_object('QualtricsContacts');
      if ($qtapi != null) {
        $result = $qtapi->getLists();

        if (isset($result->result)) {
          foreach ($result->result->elements as $list) {

            $lists[$list->id] = $list;

          }
        }

        uasort($lists, '_qualtricsapi_list_cmp');

        $cache->set('contactlists', $lists);
      }
    }
    catch (Exception $e) {
      \Drupal::logger('qualtricsapi')->error('An error occurred requesting mailing list information from Qualtrics. "{message}"', array(
        'message' => $e->getMessage()));
    }
  }

  // Filter by given IDs.
  if (!empty($list_ids)) {
    $filtered_lists = array();

    foreach ($list_ids as $id) {
      if (array_key_exists($id, $lists)) {
        $filtered_lists[$id] = $lists[$id];
      }
    }

    return $filtered_lists;
  }
  else {
    return $lists;
  }
}

/**
 * Helper function used by uasort() to sort lists alphabetically by name.
 *
 * @param array $a
 *   An array representing the first list.
 * @param array $b
 *   An array representing the second list.
 *
 * @return int
 *   One of the values -1, 0, 1
 */
function _qualtricsapi_list_cmp($a, $b) {
  if ($a->name == $b->name) {
    return 0;
  }

  return ($a->name < $b->name) ? -1 : 1;
}


/**
 * Get the Qualtrics content info for a given contact ID and list.
 *
 * Results are cached in the cache_qualtricsapi bin.
 *
 * @param string $list_id
 *   The Qualtrics list ID to get contact info for.
 * @param string $contact_id
 *   The Qualtrics contact ID to load contact info for.
 * @param bool $reset
 *   Set to TRUE if contact info should not be loaded from cache.
 *
 * @return object
 *   Contact info object, empty if there is no valid info.
 */
function qualtricsapi_get_contactinfo($list_id, $contact_id, $reset = FALSE) {
  $cache = \Drupal::cache('qualtricsapi');

  if (!$reset) {
    $cached_data = $cache->get($list_id . '-' . $contact_id);

    if ($cached_data) {
      return $cached_data->data;
    }
  }

  // Query lists from the MCAPI and store in cache:
  $contactinfo = new stdClass();

   /* @var \Qualtrics\QualtricsContacts $mcapi */
  $qt_lists = qualtricsapi_get_api_object('QualtricsContacts');

  try {
    if (!$qt_lists) {
      throw new Exception('Cannot get member info without Qualtrics API. Check API key has been entered.');
    }
    $result = $qt_lists->getContactInfo($list_id, $contact_id);
    if (!empty($result->id)) {
      $contactinfo = $result;
      $cache->set($list_id . '-' . $contact_id, $contactinfo);
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred requesting memberinfo for {email} in list {list}. "{message}"', array(
      '@email' => $email,
      '@list' => $list_id,
      '%message' => $e->getMessage(),
    ));
  }

  return $contactinfo;
}

/**
 * Check if the given email is subscribed to the given list.
 *
 * Simple wrapper around qualtricsapi_get_memberinfo().
 *
 * @param string $list_id
 *   Unique string identifier for the list on your MailChimp account.
 * @param string $contact_id
 *    The Qualtrics contact ID to load contact info for.
 * @param bool $reset
 *   Set to TRUE to ignore the cache. (Used heavily in testing functions.)
 *
 * @return bool
 *   TRUE if subscribed, FALSE otherwise.
 */
function qualtricsapi_is_subscribed($list_id, $contact_id, $reset = FALSE) {
  $subscribed = FALSE;
  $contactinfo = qualtricsapi_get_memberinfo($list_id, $contact_id, $reset);
  if (!isset($contactinfo->unsubscribed) || $contactinfo->unsubscribed === FALSE) {
    $subscribed = TRUE;
  }

  return $subscribed;
}


/**
 * Wrapper around QualtricsContacts::addContact().
 *
 * @see QualtricsContacts::addContact()
 */
function qualtricsapi_add_contact($list_id, $email, $drupal_id, $parameters = []) {
  $config = \Drupal::config('qualtricsapi.settings');
  $result = FALSE;

  try {
    /* @var \Qualtrics\QualtricsContacts $qt_lists */
    $qt_lists = qualtricsapi_get_api_object('QualtricsContacts');
    if (!$qt_lists) {
      throw new Exception('Cannot add a contact without Qualtrics API. Check API key has been entered.');
    }

    $parameters['externalDataRef'] = $drupal_id;

    // Add member to list.
    $result = $qt_lists->addContact($list_id, $email, $parameters);

    if (isset($result->result->id)) {
      \Drupal::moduleHandler()->invokeAll('qualtricsapi_add_contact_success', array($list_id, $email, $parameters, $result));

      // Clear user cache, just in case there's some cruft leftover:
      qualtricsapi_cache_clear_contact($list_id, $result->result->id);

      \Drupal::logger('qualtricsapi')->notice('{email} was added to list {list}.', array(
        'email' => $email,
        'list' => $list_id,
      ));
    }
    else {
      if (!$config->get('test_mode')) {
        \Drupal::logger('qualtricsapi')->warning('A problem occurred adding {email} to list {list}.', array(
          'email' => $email,
          'list' => $list_id,
        ));
      }
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred adding {email} to list {list}. "{message}"', array(
      'email' => $email,
      'list' => $list_id,
      'message' => $e->getMessage(),
    ));
  }

  return $result;
}




/**
 * Wrapper around QualtricsContacts::updateContact().
 *
 * @see QualtricsContacts::updateContact()
 */
function qualtricsapi_update_contact($list_id, $contact_id, $parameters) {
  $result = FALSE;

  try {
    /* @var \Qualtrics\QualtricsContacts $qt_lists */
    $qt_lists = qualtricsapi_get_api_object('QualtricsContacts');

    // Update member.
    $result = $qt_lists->updateContact($list_id, $contact_id, $parameters);

    if (isset($result->id)) {
      \Drupal::logger('qualtricsapi')->notice('{contact_id} was updated in list {list_id}.', array(
        'contact_id' => $contact_id,
        'list' => $list_id,
      ));

      // Clear user cache:
      qualtricsapi_cache_clear_contact($list_id, $contact_id);
    }
    else {
      \Drupal::logger('qualtricsapi')->warning('A problem occurred updating {contact_id} on list {list}.', array(
        'contact_id' => $contact_id,
        'list' => $list_id,
      ));
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred updating {contact_id} on list {list}. "{message}"', array(
      'contact_id' => $contact_id,
      'list' => $list_id,
      'message' => $e->getMessage(),
    ));
  }

  return $result;
}

/**
 * Retrieve all members of a given list with a given status.
 *
 * Note that this function can cause locking an is somewhat slow. It is not
 * recommended unless you know what you are doing! See the Qualtrics documentation.
 * Skip Token my be necessary to get the full list.
 */
function qualtricsapi_get_contacts($list_id, $parameters = []) {
  $results = FALSE;

  $lock = \Drupal::lock();

  if ($lock->acquire('qualtricsapi_get_contacts', 60)) {
    try {
      /* @var \Qualtrics\QualtricsContacts $qtapi */
      $qtapi = qualtricsapi_get_api_object('QualtricsContacts');

      $results = $qtapi->getContacts($list_id, $parameters);
    }
    catch (Exception $e) {
      \Drupal::logger('qualtricsapi')->error('An error occurred pulling contact info for a list. "{message}"', array(
        'message' => $e->getMessage()));
    }

    $lock->release('qualtricsapi_get_contacts');
  }

  return $results;
}


/**
 * Unsubscribes a member from a Qualtrics mailing list.
 *
 * @see Mailchimp_Lists::unsubscribe()
 */
function qualtricsapi_remove_contact($list_id, $contact_id) {
  try {
    /* @var \Qualtrics\QualtricsContacts $qt_lists */
    $qt_lists = qualtricsapi_get_api_object('QualtricsContacts');
    if (!$qt_lists) {
      throw new Exception('Cannot remove contact without Qualtrics API. Check API key has been entered.');
    }

    $qt_lists->removeContact($list_id, $contact_id);

    \Drupal::moduleHandler()->invokeAll('qualtricsapi_remove_contact_success', array($list_id, $contact_id));

    // Clear user cache:
    qualtricsapi_cache_clear_contact($list_id, $contact_id);

    return TRUE;
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred removing {contact_id} from list {list}. "{message}"', array(
      'contact_id' => $contact_id,
      'list' => $list_id,
      'message' => $e->getMessage(),
    ));
  }

  return FALSE;
}


/**
 * Returns all Qualtrics surveys for a given key. Lists are stored in the cache.
 *
 * @param array $survey_ids
 *   An array of list IDs to filter the results by.
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return array
 *   An array of list data arrays.
 */
function qualtricsapi_get_surveys($survey_ids = array(), $reset = FALSE) {
  $surveys = array();
  $cache = \Drupal::cache('qualtricsapi');
  $cached_data = $reset ? NULL : $cache->get('surveys');

  // Return cached lists.
  if ($cached_data) {
    $surveys = $cached_data->data;
  }
  else {
    try {
      /* @var \Qualtrics\QualtricsSurveys $qt_surveys */
      $qt_surveys = qualtricsapi_get_api_object('QualtricsSurveys');
      if ($qt_surveys != null) {
        $result = $qt_surveys->getSurveys();
        if (isset($result->result)) {
          foreach ($result->result->elements as $survey) {
            $surveys[$survey->id] = $survey;
          }
        }
        uasort($surveys, '_qualtricsapi_list_cmp');
        $cache->set('surveys', $surveys);
      }
    }
    catch (Exception $e) {
      \Drupal::logger('qualtricsapi')->error('An error occurred requestingsurvey information from Qualtrics. "{message}"', array(
        'message' => $e->getMessage()));
    }
  }

  // Filter by given IDs.
  if (!empty($survey_ids)) {
    $filtered_lists = array();

    foreach ($survey_ids as $id) {
      if (array_key_exists($id, $surveys)) {
        $filtered_lists[$id] = $surveys[$id];
      }
    }

    return $filtered_lists;
  }
  else {
    return $surveys;
  }
}


/**
 * Returns all Qualtrics library messages for a given user or group. Lists are stored in the cache.
 *
 * @param array $survey_ids
 *   An array of list IDs to filter the results by.
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return array
 *   An array of list data arrays.
 */
function qualtricsapi_get_library_messages($library_id = '', $reset = FALSE) {

  $messages = array();
  $cache = \Drupal::cache('qualtricsapi');
  $cached_data = $reset ? NULL : $cache->get('librarymessages');

  // Return cached lists.
  if ($cached_data) {
    $messages = $cached_data->data;
  }
  else {
    try {
      /* @var \Qualtrics\QualtricsLibraries $qt_messages */
      $qt_messages = qualtricsapi_get_api_object('QualtricsLibraries');
      if ($qt_messages != null) {
        if (empty($library_id)) {
          $library_id = $qt_messages->getAccount()->result->id;
        }
        $result = $qt_messages->getLibraryMessages($library_id);
        if (isset($result->result)) {
          foreach ($result->result->elements as $message) {
            $messages[$message->id] = $message;
          }
        }
        uasort($messages, '_qualtricsapi_list_cmp');
        $cache->set('librarymessages', $messages);
      }
    }
    catch (Exception $e) {
      \Drupal::logger('qualtricsapi')->error('An error occurred requesting library message information from Qualtrics. "{message}"', array(
        'message' => $e->getMessage()));
    }
  }

  return $messages;

}


/**
 * Wrapper around QualtricsDistributions->getDistributions() to return data for a given distribution.
 *
 * Data is stored in the Qualtrics cache.
 *
 * @param string $survey_id
 *   Survey ID for this distribution.
 * @param array $parameters
 *   Optional paramters to refine the disribution list for a survey.
 * @param bool $reset
 *   Set to TRUE if distribution data should not be loaded from cache.
 *
 * @return mixed
 *   Array of distribution data or FALSE if not found.
 */
function qualtricsapi_get_distributions($survey_id, $parameters = [], $reset = FALSE) {
  $cache = \Drupal::cache('qualtricsapi');

  $distributions = FALSE;

  if (!$reset) {
    $cached_data = $cache->get('distributions_for_survey_' . $survey_id);

    if ($cached_data) {
      return $cached_data->data;
    }
  }

  try {
    /* @var \Qualtrics\QualtricsDistributions $qtapi */
    $qtapi = qualtricsapi_get_api_object('QualtricsDistributions');

    $response = $qtapi->getDistributions($survey_id, $parameters);

    if (!empty($response->result->elements)) {
      $distributions = $response;
      $cache->set('distributions_for_survey_' . $survey_id, $distributions);
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred retrieving distribution data for survey {survey_id}. "{message}"', array(
      'survey_id' => $survey_id,
      'message' => $e->getMessage(),
    ));
  }

  return $distributions;
}

/**
 * Wrapper around QualtricsDistributions->getDistribution() to return data for a given distribution.
 *
 * Data is stored in the Qualtrics cache.
 *
 * @param string $distribution_id
 *   The ID of the distribution to get data for.
 * @param string $survey_id
 *   Survey ID for this distribution.
 * @param array $parameters
 *
 * @param bool $reset
 *   Set to TRUE if distribution data should not be loaded from cache.
 *
 * @return mixed
 *   Array of distribution data or FALSE if not found.
 */
function qualtricsapi_get_distribution_data($distribution_id, $survey_id, $reset = FALSE) {
  $cache = \Drupal::cache('qualtricsapi');

  $distribution_data = FALSE;

  if (!$reset) {
    $cached_data = $cache->get('distribution_' . $distribution_id);

    if ($cached_data) {
      return $cached_data->data;
    }
  }

  try {
    /* @var \Qualtrics\QualtricsDistributions $qtapi */
    $qtapi = qualtricsapi_get_api_object('QualtricsDistributions');

    $response = $qtapi->getDistribution($distribution_id, $survey_id);

    if (!empty($response->id)) {
      $distribution_data = $response;
      $cache->set('distribution_' . $distribution_id, $distribution_data);
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred retrieving distribution data for {distribution}. "{message}"', array(
      'distribution' => $distribution_id,
      'message' => $e->getMessage(),
    ));
  }

  return $distributions;
}


/**
 * Wrapper around QualtricsDistributions->addSurveyDistribution() to create and send a distribution.
 *
 * @param array $survey_link
 *   Survey and distro type info.
 * @param array $header
 *   The subject, from name, reply-to, etc settings for the campaign.
 * @param array $message
 *   Contains the messageId and libraryId.
 * @param array $recipients
 *   contains mailingListId or contactId.
 * @param string $send_date
 *   Time to send the email.
 *
 * @return mixed
 *   Array of distribution data or FALSE if not found.
 */
function qualtricsapi_add_distribution($survey_link, $header, $message, $recipients, $send_date) {

  $config = \Drupal::config('qualtricsapi.settings');
  $result = FALSE;

  try {
    /* @var \Qualtrics\QualtricsContacts $qt_distributions */
    $qt_distributions = qualtricsapi_get_api_object('QualtricsDistributions');
    if (!$qt_distributions) {
      throw new Exception('Cannot add a contact without Qualtrics API. Check API key has been entered.');
    }

    // Add member to list.
    $result = $qt_distributions->addSurveyDistribution($survey_link, $header, $message, $recipients, $send_date);

    if (isset($result->result->id)) {
      \Drupal::moduleHandler()->invokeAll('qualtricsapi_add_distribution_success', array($result));

      // Clear user cache, just in case there's some cruft leftover:
      qualtricsapi_cache_clear_distribution($result->result->id);

      \Drupal::logger('qualtricsapi')->notice('{distribution_id} was added in Qualtrics.', array(
        'distribution_id' => $result->result->id,
      ));
    }
    else {
      if (!$config->get('test_mode')) {
        \Drupal::logger('qualtricsapi')->warning('A problem occurred adding a new distribution for survey {survey}.', array(
          'survey' => $survey_link['surveyId'],
        ));
      }
    }
  }
  catch (Exception $e) {
    \Drupal::logger('qualtricsapi')->error('An error occurred adding a new distribution for survey {survey}. "{message}"', array(
      'survey' => $survey_link['surveyId'],
      'message' => $e->getMessage(),
    ));
  }

  return $result;

}


/**
 * Clears a qualtricsapi user member info cache.
 *
 * @param string $list_id
 * @param string $contact_id
 */
function qualtricsapi_cache_clear_contact($list_id, $contact_id) {
  $cache = \Drupal::cache('qualtricsapi');
  $cache->deleteAll($list_id . '-' . $contact_id);
}

/**
 * Clears a qualtricsapi activity cache.
 *
 * @param string $campaign_id
 */
function qualtricsapi_cache_clear_distribution($distribution_id) {
  $cache = \Drupal::cache('qualtricsapi');
  $cache->deleteAll('distribution_' . $distribution_id);
}

/**
 * Implements hook_flush_caches().
 */
function qualtricsapi_flush_caches() {
  return array('qualtricsapi');
}


/**
 * Implements hook_cron().
 *
 * Processes queued MailChimp actions.
 */
function qualtricsapi_cron() {
  // $queue = \Drupal::queue(MAILCHIMP_QUEUE_CRON);
  // $queue->createQueue();

  // $queue_count = $queue->numberOfItems();

  // if ($queue_count > 0) {
  //   $config = \Drupal::config('qualtricsapi.settings');

  //   $batch_limit = $config->get('batch_limit');
  //   $batch_size = ($queue_count < $batch_limit) ? $queue_count : $batch_limit;
  //   $count = 0;

  //   while ($count < $batch_size) {
  //     if ($item = $queue->claimItem()) {
  //       call_user_func_array($item->data['function'], $item->data['args']);
  //       $queue->deleteItem($item);
  //     }

  //     $count++;
  //   }
  // }
}

/**
 * Wrapper for email validation function in core.
 *
 * Necessary so email validation function can be added
 * to forms as a value in the #element_validate array.
 *
 * @see \Egulias\EmailValidator\EmailValidator::isValid()
 */
function qualtricsapi_validate_email($mail) {
  return \Drupal::service('email.validator')->isValid($mail['#value']);
}
