<?php

/**
 * @file
 * Qualtrics Module Drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function qualtricsapi_drush_command() {
  $items = array();

  $items['qualtricsapi-cron'] = array(
    'callback' => '_qualtricsapi_cron_batch',
    'description' => "Trigger Qualtrics cron task (PLACEHOLDER)",
    'examples' => array(
      'drush qualtricsapi-cron' => 'Run a Qualtrics cron task.',
    ),
  );

  return $items;
}

/**
 * Callback function to run cron.
 *
 * @int null $temp_batchsize
 */
function _qualtricsapi_cron_batch() {

  qualtricsapi_cron();

}
