<?php

namespace Drupal\qualtricsapi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Qualtrics API settings for this site.
 */
class QualtricsApiAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'qualtricsapi_admin_settings';
  }

  protected function getEditableConfigNames() {
    return ['qualtricsapi.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qualtricsapi.settings');

    $qt_api_url = Url::fromUri('https://co1.qualtrics.com/ControlPanel', array('attributes' => array('target' => '_blank')));
    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Qualtrics API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#description' => t('The API key for your Qualtrics account. Get or generate a valid API key at your @apilink.',
        array('@apilink' => \Drupal::l(t('Qualtrics API Dashboard'), $qt_api_url))),
    );

    $form['api_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Qualtrics API User ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_user'),
      '#description' => t('The User ID your Qualtrics account. Get from your API account.',
        array('@apilink' => \Drupal::l(t('Qualtrics API Dashboard'), $qt_api_url))),
    );

    // Display a message to confirm connection right away.
    $lists = qualtricsapi_get_mailing_lists(array(), TRUE);
    if (!empty($lists)) {
      drupal_set_message(t('You are connected to Qualtrics.'), 'status');
    } else {
      drupal_set_message(t('You are NOT connected to Qualtrics.'), 'warning');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('qualtricsapi.settings');
    $config
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_user', $form_state->getValue('api_user'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
