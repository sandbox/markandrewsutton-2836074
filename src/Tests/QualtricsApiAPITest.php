<?php

namespace Drupal\qualtricsapi\Tests;

/**
 * Tests core API functionality.
 *
 * @group qualtricsapi
 */
class QualtricsApiAPITest extends QualtricsApiTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('qualtricsapi', 'qualtricsapi_test');

  /**
   * Tests that the test API has been loaded.
   */
  function testAPI() {
    $qualtricsapi_api = qualtricsapi_get_api_object();

    $this->assertNotNull($qualtricsapi_api);

    $this->assertEqual(get_class($qualtricsapi_api), 'Qualtrics\Tests\Qualtrics');
  }

}
