<?php

namespace Drupal\qualtricsapi\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\qualtricsapi_test\QualtricsApiConfigOverrider;

$path = drupal_get_path('module', 'qualtricsapi');

include_once $path . "/lib/qualtricsapi-api-php/tests/src/Client.php";
include_once $path . "/lib/qualtricsapi-api-php/tests/src/Qualtrics.php";
include_once $path . "/lib/qualtricsapi-api-php/tests/src/Response.php";

/**
 * Sets up MailChimp module tests.
 */
abstract class QualtricsApiTestBase extends WebTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    // Use a profile that contains required modules:
    $this->profile = $this->originalProfile;

    parent::setUp();

    \Drupal::configFactory()->addOverride(new QualtricsApiConfigOverrider());
  }

}
