This module provides integration with the Qualtrics survey service.

The core module provides basic configuration and API integration.

## Features
  * API integration

## Installation Notes
  * You need to have a Qualtrics API Key.
  * You need to have a Qualtrcs User ID.


## Configuration
  1. Direct your browser to admin/config/services/qualtricsapi to configure the
  module.

  2. You will need to put in your Qualtrics API key for your Qualtrics account.
  Once you have set up your account and are logged in, visit:
  Account Settings -> Qualtrics IDs to generate a key.

  3. Copy your newly created API key and go to the
  [Qualtrics API config](http://example.com/admin/config/services/qualtricsapi) page in
  your Drupal site and paste it into the Qualtrics API Key field.

  4. Copy your User ID Qualtrics and paste it in the User ID field on the [Qualtrics API config](http://example.com/admin/config/services/qualtricsapi) page in your Drupal site
